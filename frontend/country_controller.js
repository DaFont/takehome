'use strict';

App.controller('CountryController', ['$scope', 'Country', function($scope, Country) {
    var self = this;
    self.country= new Country();

    self.countries=[];

    self.fecthAllCountries = function(){
        self.countries = Country.query();
    };

    self.createCountry = function(){
        self.country.$save(function(){
            self.fecthAllCountries();
        });
    };

    self.updateCountry = function(){
        self.country.$update(function(){
            self.fecthAllCountries();
        });
    };

    self.deleteCountry = function(identity){
        var country = Country.get({id:identity}, function() {
            country.$delete(function(){
                console.log('Deleting country with id ', identity);
                self.fecthAllCountries();
            });
        });
    };

    self.fecthAllCountries();

    self.submit = function() {
        if(self.country.id==null){
            console.log('Saving New Country', self.country);
            self.createCountry();
        }else{
            console.log('Upddating Country with id ', self.country.id);
            self.updateCountry();
            console.log('Country updated with id ', self.country.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.countries.length; i++){
            if(self.countries[i].id === id) {
                self.country = angular.copy(self.countries[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.country.id === id) {//If it is the one shown on screen, reset screen
            self.reset();
        }
        self.deleteCountry(id);
    };


    self.reset = function(){
        self.country= new Country();
        $scope.myForm.$setPristine(); //reset Form
    };

}]);