package com.dfont.apirest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class LoadDatabase {

    /*CommandLineRunner is executed after context is loaded,
    so we can save some fake data to full our DB*/
    @Bean
    CommandLineRunner initDatabase(CountryRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new Country("España", 42000000)));
            log.info("Preloading " + repository.save(new Country("Egipto", 95690000)));
        };
    }
}
