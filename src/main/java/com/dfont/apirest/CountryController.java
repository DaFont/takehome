package com.dfont.apirest;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin //This tag by default allows requests from anywhere.
@RestController /*This tag includes the @Controller and @ResponseBody,simplifies the controller implementation:*/
public class CountryController {

    private final CountryRepository repository;

    CountryController(CountryRepository repository) {
        this.repository = repository;
    }

    //Aggregate root
    @GetMapping("/countries")
    List<Country> all() {
        return repository.findAll();
    }

    @PostMapping("/countries")
    Country newCountry(@RequestBody @Valid Country newCountry) {
        return repository.save(newCountry);
    }

    //Single item
    @GetMapping("/countries/{id}")
    Country one(@PathVariable Long id) {
        return repository.findById(id)
                .orElseThrow(RuntimeException::new);
    }

    @PutMapping("/countries/{id}")
    Country replaceCountry(@RequestBody @Valid Country newCountry, @PathVariable Long id) {

        return repository.findById(id)
                .map(country -> {
                            country.setCountryName(newCountry.getCountryName());
                            country.setPopulation(newCountry.getPopulation());
                    return repository.save(country);
                        })
                .orElseGet(() -> {
                    newCountry.setId(id);
                    return repository.save(newCountry);
                });
    }


    @DeleteMapping("/countries/{id}")
    void deleteCountry(@PathVariable Long id){
        repository.deleteById(id);
    }

}
