**REST API WITH SPRING AND JAVA 1.8**

In this project we build a REST API using different technologies.

This API can manage all CRUD operations in DB trough http requests.

We use Spring framework to boost our implementation along other interesting libraries.

---

**DEPLOY AND RUN**
1. To use this project the first step is clone this repo: git clone https://DaFont@bitbucket.org/DaFont/takehome.git

2. Once you have clone the repo move to the root folder an execute: mvn spring-boot:run this build the project and run it.

3. Check if all is OK open this URL on your favorite browser: http://localhost:8080/countries, you should view a response with two countries.

---

**Documentation**
When app is running Swagger UI Documentation can found on: http://localhost:8080/swagger-ui.html

**Frontend**
We code a little angularjs frontend to consumes our api, just open frontend folder and make doble click on index.html to use it.